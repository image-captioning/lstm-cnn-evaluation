import numpy as np
from numpy import array
import pandas as pd
import matplotlib.pyplot as plt
import string
import os
from PIL import Image
import glob
from pickle import dump, load
from time import time

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import LSTM, Embedding, TimeDistributed, Dense, RepeatVector,\
                         Activation, Flatten, Reshape, concatenate, Dropout, BatchNormalization
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import Bidirectional
from keras.layers.merge import add
from keras.applications.inception_v3 import InceptionV3
from keras.preprocessing import image
from keras.models import Model
from keras import Input, layers
from keras import optimizers
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

# read file caption
def load_doc(fileName):
    file = open(fileName, 'r')
    text = file.read()
    file.close()
    return text


def save_descriptions(descriptions):
    lines = []
    for key, desc in descriptions.items():
        for de in desc:
            line = key + ' ' + de
            lines.append(line)
    data = '\n'.join(lines)
    file = open('data_save', 'w')
    file.write(data)
    file.close()


def clean_descriptions(descriptions):
    # prepare translation table for removing punctuation
    table = str.maketrans('', '', string.punctuation)
    for key, desc_list in descriptions.items():
        for i in range(len(desc_list)):
            desc = desc_list[i]
            # tokenize
            desc = desc.split()
            # convert to lower case
            desc = [word.lower() for word in desc]
            # remove punctuation from each token
            desc = [w.translate(table) for w in desc]
            # remove hanging 's' and 'a'
            desc = [word for word in desc if len(word)>1]
            # remove tokens with numbers in them
            desc = [word for word in desc if word.isalpha()]
            # store as string
            desc_list[i] =  ' '.join(desc)
    return descriptions


# Lưu caption dưới dạng key value: id_image : ['caption 1', 'caption 2', 'caption 3',' caption 4', 'caption 5']
def load_description(fileName):
    doc = load_doc(fileName)
    mapping = dict()
    for line in doc.split('\n'):
        tokens = line.split()
        if len(tokens) < 2:
            continue
        img_id, img_desc = tokens[0], tokens[1:]
        img_name = img_id.split('.')[0]
        img_desc = ' '.join(img_desc)
        if img_name not in mapping:
            mapping[img_name] = []
        mapping[img_name].append(img_desc)
    mapping = clean_descriptions(mapping)
    save_descriptions(mapping)
    return mapping


def load_data_save(path):
    file = open(path, 'r')
    data = file.read()
    file.close()
    dict_data = {}
    for line in data.split('\n'):
        image_name, caption = line.split()[0], line.split()[1:]
        caption = ' '.join(caption)
        if image_name not in dict_data:
            dict_data[image_name] = []
        dict_data[image_name].append(caption)

    return dict_data

# load image data tran, test, val
def load_set(file_name):
    doc = load_doc(file_name)
    dataset = []
    for line in doc.split('\n'):
        if len(line) < 1:
            continue
        dataset.append(line.split('.')[0])
    return dataset

def load_set_extent_jpg(file_name):
    doc = load_doc(file_name)
    dataset = []
    for line in doc.split('\n'):
        if len(line) < 1:
            continue
        dataset.append(line)
    return dataset


#Thêm 'startseq', 'endseq' cho chuỗi
def load_clean_descriptions(filename, dataset):
    doc = load_doc(filename)
    descriptions = dict()
    for line in doc.split('\n'):
        # split line by white space
        tokens = line.split()
        # split id from description
        image_id, image_desc = tokens[0], tokens[1:]
        # skip images not in the set
        if image_id in dataset:
            # create list
            if image_id not in descriptions:
                descriptions[image_id] = list()
            # wrap description in tokens
            desc = 'startseq ' + ' '.join(image_desc) + ' endseq'
            # store
            descriptions[image_id].append(desc)
    return descriptions















