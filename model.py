from prepare_data_image import *
from prepare_data_text import *
import tensorflow as tf


path_of_data = 'data_save'
description = load_data_save(path_of_data)

#print(description)

file_train = 'Flickr8k_text/Flickr_8k.trainImages.txt'
file_test = 'Flickr8k_text/Flickr_8k.testImages.txt'
file_val = 'Flickr8k_text/Flickr_8k.devImages.txt'

# 6000 image-caption
train_set = load_set(file_train)
# 1000 image-caption
test_set = load_set(file_test)
val_set = load_set(file_val)

train_set_jpg = load_set_extent_jpg(file_train)
test_set_jpg = load_set_extent_jpg(file_test)
val_set_jpg = load_set_extent_jpg(file_val)


# Thêm 'startseq', 'endseq' cho chuỗi
train_descriptions = load_clean_descriptions('data_save', train_set)
backbone = tf.keras.applications.InceptionV3(weights="imagenet")
backbone = Model(backbone.input, backbone.layers[-2].output)

path_folder_image = 'Flickr8k_Dataset/Flicker8k_Dataset/'

encoding_train = generate_feature_vector(path_folder_image, train_set_jpg, backbone)
with open("Flickr8k/Pickle/encoded_train_images.pkl", "wb") as encoded_pickle:
    dump(encoding_train, encoded_pickle)






print(train_set)

























